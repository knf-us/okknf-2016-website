import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons/faFacebookSquare'
import { faEnvelope } from '@fortawesome/free-regular-svg-icons/faEnvelope'
import { faCalendarAlt } from '@fortawesome/free-regular-svg-icons/faCalendarAlt'
import { faMoneyBillAlt } from '@fortawesome/free-regular-svg-icons/faMoneyBillAlt'
import { faFilePdf } from '@fortawesome/free-regular-svg-icons/faFilePdf'
import { faEye } from '@fortawesome/free-regular-svg-icons/faEye'
import { faHandSpock } from '@fortawesome/free-regular-svg-icons/faHandSpock'
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons/faMapMarkerAlt'
import { faGraduationCap } from '@fortawesome/free-solid-svg-icons/faGraduationCap'
import { faBed } from '@fortawesome/free-solid-svg-icons/faBed'
import { faTaxi } from '@fortawesome/free-solid-svg-icons/faTaxi'
import React from 'react'

import { Info } from './Info'
import { Participants } from './Participants'
import { Patrons } from './Patrons'
import { Contacts } from './Contacts'
import { Footer, LandingPage, Menu } from './Layout'

library.add(
  faFacebookSquare,
  faEnvelope,
  faCalendarAlt,
  faMoneyBillAlt,
  faFilePdf,
  faEye,
  faHandSpock,
  faMapMarkerAlt,
  faGraduationCap,
  faBed,
  faTaxi
)

const App = () => (
  <div>
    <Menu />
    <LandingPage />
    <Info />
    <Participants />
    <Patrons />
    <Contacts />
    <Footer />
  </div>
)

export default App

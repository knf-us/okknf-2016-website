import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import PropTypes from 'prop-types'
import React from 'react'
import { Col, Container, Row } from 'reactstrap'

import { ContactMap } from './Maps'

export const Contacts = () => (
  <div id="contact" className="chapter-wrapper">
    <Container>
      <Row>
        <Col lg={12} md={12} sm={12} xs={12}>
          <h1>Kontakt</h1>
          <h2>Koło Naukowe Fizyków UŚ</h2>
          <ul className="fa-ul contacts">
            <li><a href="mailto:knf@smcebi.edu.pl?Subject=Ważne%20rzeczy" target="_top">
              <FontAwesomeIcon icon={['far', 'envelope']} /> knf@smcebi.edu.pl
            </a></li>
            <li><a href="https://www.facebook.com/kolonaukowefizykowUS">
              <FontAwesomeIcon icon={['fab', 'facebook-square']} /> kolonaukowefizykowUS
            </a></li>
            <li><a href="http://www.openstreetmap.org/?mlat=50.29474&mlon=18.93370#map=19/50.29474/18.93370&layers=N">
              <FontAwesomeIcon icon={['fas', 'map-marker-alt']} /> SMCEBI, p. G/0/06
            </a></li>
          </ul>
        </Col>
        <Col id='contact-map' lg={12} md={12} sm={12} xs={12}>
          <ContactMap />
        </Col>
      </Row>
    </Container>
  </div>
)

import PropTypes from 'prop-types'
import React from 'react'
import { Col, Container, Row } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { HotelMap } from './Maps'

const InfoCard = ({ icon, title, children }) => (
  <Col lg={4} md={6} sm={6} xs={12}>
    <div className='text-center'>
      {icon}
      <h2>{title}</h2>
    </div>
    <div>
      {children}
    </div>
  </Col>
)
InfoCard.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired
}

export const Info = () => (
  <div id="info" className="chapter-wrapper">
    <Container>
      <Row className="mb-4">
        <Col lg={12} md={12} sm={12} xs={12}>
          <h1>Informacje</h1>
          <p>
            Organizowana przez Koło Naukowe Fizyków Uniwersytetu Śląskiego konferencja jest kolejną
            edycją spotkań naukowych, w których udział biorą studenci oraz pracownicy naukowi zajmujący
            się fizyką zarówno zawodowo jak i&nbsp;hobbystycznie. Sesje wykładowe poświęcone są
            zagadnieniom fizyki oraz jej metodom matematycznym i&nbsp;komputerowym. Poprzez poszerzanie
            swoich horyzontów myślowych oraz poznawanie nowych technologii uczestnicy konferencji
            zdobywają wiedzę, którą mogą wykorzystać w swojej późniejszej karierze zarówno naukowej,
            jak i&nbsp;zawodowej, co sprzyja rozwojowi dziedzin takich jak nauka i&nbsp;technika.
            Tegoroczna edycja objęta jest patronatem honorowym <a href='http://www.um.zabrze.pl/mieszkancy/samorzad/prezydent-miasta'>
            Prezydenta Miasta Zabrze, Małgorzaty Mańki-Szulik</a>, <a href='http://www.banys.us.edu.pl/'>
            JM Rektora Uniwersytetu Śląskiego w Katowicach prof. zw. dra hab. Wiesława Banysia</a>,
            oraz <a href='http://w3.mfc.us.edu.pl/index.php?strona=wladze'>Dziekana Wydziału
            Matematyki, Fizyki i&nbsp;Chemii Uniwersytetu Śląskiego, prof. zw. dr hab. Alicji
            Ratusznej</a>.
          </p>
          <p>
            Każdego roku sympozjum cieszy się dużym zainteresowaniem - w&nbsp;poprzednich edycjach
            uczestniczyło w nim do 80 osób, które wygłosiły liczne referaty i&nbsp;prezentowały plakaty,
            a&nbsp;przez ostatnie 14 lat w&nbsp;konferencji wzięło udział blisko 1000 studentów. Podczas
            sesji wykładowych uczestnicy mają możliwość zaprezentowania własnych osiągnięć, jak również
            zapoznania się z&nbsp;dorobkiem naukowym pozostałych uczestników. W&nbsp;corocznych
            spotkaniach biorą również udział przedstawiciele polskiej nauki, dzieląc się swoją wiedzą
            i&nbsp;doświadczeniem.
          </p>
        </Col>
      </Row>
      <Row>
        <InfoCard title='Ważne terminy' icon={<FontAwesomeIcon icon={['far', 'calendar-alt']} size='5x' />}>
          <ul style={{marginLeft: '20px'}}>
            <li>4-31 maja - rejestracja uczestników</li>
            <li>28 maja - ostateczny termin zgłaszania abstraktów i&nbsp;wpłacania opłat konferencyjnych</li>
            <li>4-6 czerwca - termin trwania konferencji</li>
          </ul>
          <p className='text-info'>
            Prosimy o przesyłanie abstractów na adres mailowy OKKNF 2016, preferowaną formą
            abstractów jest $\LaTeX$ lub zwykły plik tekstowy (preferowane kodowanie: UTF-8).
          </p>
        </InfoCard>
        <InfoCard title='Opłata konferencyjna' icon={<FontAwesomeIcon icon={['far', 'money-bill-alt']} size='5x' />}>
          <p>Wysokość opłaty konferencyjnej wynosi 290&nbsp;zł. Prosimy o wpłaty na konto:</p>
          <p className='text-center'>
            Uniwersytet Śląski w&nbsp;Katowicach<br />
          ul. Bankowa 12, 40-007 Katowice<br />
            Nr konta: 74 1050 1214 1000 0007 0000 7909 (ING Bank Śląski Odział w&nbsp;Katowicach)
          </p>
          <p>
            Jeśli konieczność wpłaty na konto jest problemem prosimy o&nbsp;informację.
          </p>
          <p className='text-danger'>
            Ważne! Jeśli chesz mieć fakturę wystawioną na uczelnię to uczelnia musi dokonać wpłaty.
            W&nbsp;innym wypadku nie ma możliwości wystawienia faktury na uczelnię!
          </p>
        </InfoCard>
        <InfoCard title='Wystąpienia' icon={<FontAwesomeIcon icon={['fas', 'graduation-cap']} size='5x' />}>
          <p>
            Podczas konferencji planujemy 27 30-minutowych (20 minut prezentacji + 10 minut przewidzianych na dyskusję)
             prezentacji dotyczących tematyki z&nbsp; zakresu szeroko rozumianej fizyki, jak i&nbsp;dziedzin pokrewnych.
             Oprócz sesji referatowych zorganizowana będzie również sesja posterowa.
          </p>
          <p>
            <FontAwesomeIcon icon={['far', 'file-pdf']} /> Abstract book z planem konferencji dostępny jest do pobrania
            pod <a href='http://nbviewer.jupyter.org/github/knf-us/okknf-2016-abstract-book/blob/master/okknf-2016-abstract-book.pdf'>adresem</a>.
          </p>
          <p>
            Jak co roku dla najlepszej prezentacji i&nbsp;najlepszego postera przewidziane są nagrody pienieżne i&nbsp;rzeczowe.
          </p>
        </InfoCard>
        <InfoCard title='Czas wolny' icon={<FontAwesomeIcon icon={['far', 'eye']} size='5x' />}>
          <p>
            Ponieważ na tradycyjne wyjście w&nbsp;góry może się okazać za daleko, w&nbsp;chwili
            wolnej od wydarzeń o&nbsp;charakterze naukowym zaplanowaliśmy dla Was wycieczkę
            do <a href='https://www.kopalniaguido.pl'>kopalni Guido</a>.
          </p>
        </InfoCard>
        <InfoCard title='Hotel' icon={<FontAwesomeIcon icon={['fas', 'bed']} size='5x' />}>
          <p>
            W tym roku gościć nas będzie <a href='http://www.hotelalpex.pl/'>hotel Alpex</a> w&nbsp;Zabrzu - również
            w&nbsp;tym miejscu będą miały miejsce prezentacje uczestników, a&nbsp;także sesja posterowa.
          </p>
        </InfoCard>
        <InfoCard title='Dojazd' icon={<FontAwesomeIcon icon={['fas', 'taxi']} size='5x' />}>
          <p>
            Szczegółowy opis dojazdu komunikacją publiczną z dworca PKP w Zabrzu do hotelu Alpex
            znajduje się <a href='files/trasa.pdf'>tutaj</a>.
          </p>
        </InfoCard>
      </Row>
      <Row>
        <Col id="hotel-map" lg={12} md={12} sm={12} xs={12}>
          <HotelMap />
        </Col>
      </Row>
    </Container>
  </div>
)

import React, { Component } from 'react'
import { Col, Collapse, Container, Nav, NavItem, NavLink, Navbar, NavbarBrand, NavbarToggler, Row } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export class Menu extends Component {
  state = {
    collapsed: true
  }

  toggleNavbar = () => {
    this.setState({ collapsed: !this.state.collapsed })
  }

  hideNavbar = () => {
    this.setState({ collapsed: true })
  }

  render() {
    return (
      <Navbar id="menu" expand="lg" dark color="dark" fixed="top">
        <Container>
          <NavbarToggler onClick={this.toggleNavbar} />
          <NavbarBrand href="#" onClick={this.hideNavbar}>
            OKKNF 2016
          </NavbarBrand>
          <span id="facebook-icon" className="navbar-text">
            <a href="https://www.facebook.com/OKKNF">
              <FontAwesomeIcon icon={['fab', 'facebook-square']} size="2x" />
            </a>
          </span>
          <Collapse isOpen={!this.state.collapsed} navbar>
            <Nav navbar>
              <NavItem onClick={this.hideNavbar}>
                <NavLink href="#info">Informacje</NavLink>
              </NavItem>
              <NavItem onClick={this.hideNavbar}>
                <NavLink href="#participants">Uczestnicy</NavLink>
              </NavItem>
              <NavItem onClick={this.hideNavbar}>
                <NavLink href="#patrons">Sponsorzy</NavLink>
              </NavItem>
              <NavItem onClick={this.hideNavbar}>
                <NavLink href="#contact">Kontakt</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    )
  }
}

export const LandingPage = () => (
  <div id="landing-page" className="chapter-wrapper">
    <Container>
      <Row>
        <Col id="logo" xl={4} lg={5} md={12} sm={12} xs={12} className="text-center">
          <img src="img/okknf-2016-logo.png" alt="Logo OKKNF" />
        </Col>
        <Col xl={{ size: 7, offset: 1 }} lg={7} md={12} sm={12} xs={12}>
          <div id="motto" className="mb-4">
            <p>
              &bdquo;Naukę buduje się z faktów, tak jak dom z cegieł; lecz nagromadzenie faktów
              jeszcze nie jest nauką, podobnie jak sterta kamieni nie jest domem.&rdquo;
            </p>
            <div className="text-right">- Henri Poincaré</div>
          </div>
          <p className="mb-4">
            Zapraszamy na XV edycję Ogólnopolskiej Konferencji Kół Naukowych Fizyków, organizowaną w
            sercu Górnego Śląska - w&nbsp;Zabrzu. Wśród przewidzianych atrakcji, poza poszerzeniem
            swojej wiedzy z&nbsp;dziedziny fizyki, proponujemy poznanie wyjątkowego regionu, jakim jest
            Górny Śląsk oraz zwiedzenie Zabytkowej Kopalni Guido.
          </p>
          <p className="text-center" style={{ fontSize: '1.24em' }}>
            Termin konferencji: 4-6 czerwca 2016
          </p>
        </Col>
      </Row>
    </Container>
  </div>
)

export const Footer = () => (
  <footer>
    <Container>
      <Row>
        <Col xl={12} lg={12} md={12} sm={12} xs={12}>
          <p>
            <FontAwesomeIcon icon={["far", "hand-spock"]} /> Live long and prosper! &#169; 2018
            Design by <a href="https://gitlab.com/stormyowl">stormyowl</a>
          </p>
        </Col>
      </Row>
    </Container>
  </footer>
)

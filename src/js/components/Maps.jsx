import React from 'react'
import { Map, Marker, Popup, TileLayer } from 'react-leaflet'
import 'leaflet-sleep'

const hotelLocation = [50.3133248, 18.7666173]

export const HotelMap = () => <Map center={hotelLocation} zoom={14} sleep={true} sleepNote={true}>
  <TileLayer
    url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  />
  <Marker position={hotelLocation}>
    <Popup>
      <div>
        <span className="bold">Hotel Alpex</span><br/>
        ul. Franciszkańska 8<br/>
        41-800 Zabrze<br/>
        <a href="http://www.hotelalpex.pl/">http://www.hotelalpex.pl/</a>
      </div>
    </Popup>
  </Marker>
</Map>

const universityLocation = [50.29474, 18.93370]

export const ContactMap = () => <Map center={universityLocation} zoom={17} sleep={true} sleepNote={true}>
  <TileLayer
    url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  />
  <Marker position={universityLocation}>
    <Popup>
      <div>
        <span className="bold">Śląskie Międzyuczelniane Centrum Edukacji i Badań Interdyscyplinarnych</span><br/>
        ul. 75 Pułku Piechoty 1A<br/>
        41-500 Chorzów<br/>
        <a href="http://www.smcebi.us.edu.pl/">http://www.smcebi.us.edu.pl/</a>
      </div>
    </Popup>
  </Marker>
</Map>

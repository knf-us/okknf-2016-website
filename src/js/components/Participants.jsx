import React from 'react'
import BootstrapTable from 'react-bootstrap-table-next'
import { Col, Container, Row } from 'reactstrap'

import { participants } from '../../data/participants.json'

const columns = [{
  dataField: 'id',
  text: 'Lp.'
}, {
  dataField: 'name',
  text: 'Imię i nazwisko'
}, {
  dataField: 'affiliation',
  text: 'Uczelnia'
}, {
  dataField: 'presentation',
  text: 'Prezentacja'
}, {
  dataField: 'poster',
  text: 'Poster'
}]

const data = participants.map((x, i) => ({ id: `${i + 1}.`, ...x}))

export const Participants = () => (
  <div id="participants" className="chapter-wrapper">
    <Container>
      <h1>Uczestnicy</h1>
      <Row>
        <Col lg={12} md={12} sm={12} xs={12}>
          <div className="table-wrapper">
            <BootstrapTable
              keyField="id"
              data={data}
              columns={columns}
              bordered={false}
              hover
              condensed
            />
          </div>
        </Col>
      </Row>
    </Container>
  </div>
)

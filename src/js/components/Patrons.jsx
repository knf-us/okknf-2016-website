import PropTypes from 'prop-types'
import React from 'react'
import { Container } from 'reactstrap'

import { patrons } from '../../data/patrons.json'

const Banner = ({ url, logoPath, altText }) => (
  <div className="banner">
    <a href={url}><img src={logoPath} alt={altText} /></a>
  </div>
)

Banner.propTypes = {
  url: PropTypes.string.isRequired,
  logoPath: PropTypes.string.isRequired,
  altText: PropTypes.string.isRequired
}

export const Patrons = () => (
  <div id="patrons" className="chapter-wrapper">
    <Container>
      <h1>Sponsorzy i patronat</h1>
      <nav className="text-center">
        {patrons.map((x, key) => <Banner {...{ key, ...x } } />)}
      </nav>
    </Container>
  </div>
)

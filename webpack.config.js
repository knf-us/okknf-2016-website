const path = require('path')
const webpack = require('webpack')

const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

const devMode = process.env.NODE_ENV !== 'production'

module.exports = {
  context: __dirname,
  devtool: 'source-map',

  entry: {
    app: './src/js/main.js'
  },

  output: {
    path: path.resolve('./dist'),
    filename: 'js/[name].min.js',
    sourceMapFilename: 'js/[name].js.map'
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      { test: /\.(sa|sc|c)ss$/,
        use: [
          devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          'resolve-url-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(otf|svg|woff|woff2|ttf|eot)(\?.*$|$)/,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]',
          outputPath: '.'
        }
      },
      { test: /\.(png|jpg)$/,
        loader: 'file-loader',
        options: {
          name: 'img/[name].[ext]',
          outputPath: '.'
        }
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json']
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.template.ejs',
      inject: 'body',
    }),
    new MiniCssExtractPlugin({
      filename: '[name].min.css',
      outputPath: 'css'
    }),
    new CopyWebpackPlugin([
      { from: 'src/files', to: 'files' },
      { context: 'src/img', from: '**/*.+(png|jpg)', to: 'img' },
      { context: 'node_modules/leaflet/dist/images/', from: '*.png', to: 'img' }
    ]),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    }),
  ],
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: true,
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  }
}
